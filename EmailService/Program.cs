﻿using EmailService.Data;
using EmailService.Data.DAL;
using EmailService.Data.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;
using Xperience.SendGridClient;
using Xperience.SendGridClient.Models;

namespace EmailService
{
    public class Program
    {
        static readonly Mutex mutex = new Mutex(true, "{8299C727-40BA-4CEE-B127-D317FAEB0E26}");

        [STAThread]
        public static async Task Main()
        {
            if (mutex.WaitOne(TimeSpan.Zero, true))
            {
                var appConfig = JsonSerializer.Deserialize<AppConfig>(File.ReadAllText($"{AppDomain.CurrentDomain.BaseDirectory}\\appConfig.json"));

                Console.WriteLine(AppDomain.CurrentDomain.BaseDirectory);

                try
                {
                    await WriteLog($"Starting at {DateTime.Now}", appConfig.LogFileLocation);

                    var context = new EmailServiceContext(
                        new DbContextOptionsBuilder<EmailServiceContext>().UseSqlServer(appConfig.EmailServiceConnection).Options);
                    var emailsDAL = new EmailsDAL(context);

                    await Run(emailsDAL, new SendGridClient(new HttpClient(), appConfig.ApiKey));
                }
                catch(Exception ex)
                {
                    await WriteLog($"\t\tERROR: {JsonSerializer.Serialize(ex)}", appConfig.LogFileLocation);
                    Environment.Exit(1);
                }

                await WriteLog($"Ending at {DateTime.Now}", appConfig.LogFileLocation);
            }

            Environment.Exit(0);
        }

        private static async Task WriteLog(string log, string location)
        {
            await File.AppendAllTextAsync(location, $"\n\n{log}");
        }

        public static async Task Run(EmailsDAL emailsDAL, SendGridClient client)
        {
            if(emailsDAL is null)
            {
                throw new ArgumentNullException(nameof(emailsDAL));
            }

            if (client is null)
            {
                throw new ArgumentNullException(nameof(client));
            }

            var emails = emailsDAL.GetEmailsAsync().Result;

            foreach (var email in emails.Where(e => e.DelayUntil < DateTime.Now))
            {
                try
                {
                    if (await IsSuppressed(email, client))
                    {
                        var history = new History()
                        {
                            CreatedDateTime = DateTime.UtcNow,
                            EmailJson = JsonSerializer.Serialize(email),
                            Reason = "Address is in suppression list",
                            Sent = false
                        };

                        await emailsDAL.AddToHistory(history);
                        await emailsDAL.RemoveEmailFromQueue(email);

                        continue;
                    }

                    var fromEmail = await emailsDAL.GetFromEmailAsync(email.FromAddressId);
                    var templateEmail = new TemplateEmail(
                        email.ToAddress,
                        email.ToDisplayName,
                        fromEmail.Email,
                        fromEmail.DisplayName,
                        email.TemplateId,
                        JsonSerializer.Deserialize<dynamic>(email.TemplateData),
                        (int)email.SuppressionGroupId);

                    var response = await client.SendTemplateEmailAsync(templateEmail);

                    if (response.IsSuccessStatusCode)
                    {
                        var history = new History()
                        {
                            CreatedDateTime = DateTime.UtcNow,
                            EmailJson = JsonSerializer.Serialize(email),
                            Sent = true
                        };

                        await emailsDAL.AddToHistory(history);
                        await emailsDAL.RemoveEmailFromQueue(email);
                    }
                    else
                    {
                        if (email.SendAttempts == email.MaxRetryAttempts)
                        {
                            var poison = new PoisonQueue()
                            {
                                Email = JsonSerializer.Serialize(email),
                                PoisonedDateTime = DateTime.UtcNow,
                                PoisonMessage = "Exceeded Max Retry Attempts"
                            };

                            await emailsDAL.AddToPoisonQueue(poison);
                            await emailsDAL.RemoveEmailFromQueue(email);
                        }
                        else
                        {
                            email.SendAttempts++;
                            await emailsDAL.UpdateEmail(email);
                        }
                    }
                }
                catch (Exception ex)
                {
                    var poison = new PoisonQueue()
                    {
                        Email = JsonSerializer.Serialize(email),
                        PoisonedDateTime = DateTime.UtcNow,
                        PoisonMessage = $"Exception Thrown: {ex.Message}\n\tStackTrace:{ex.StackTrace}\n\n\n\t\tFullExObj:{ex}"
                    };

                    await emailsDAL.AddToPoisonQueue(poison);

                    if (email.SendAttempts == email.MaxRetryAttempts)
                    {
                        await emailsDAL.RemoveEmailFromQueue(email);
                    }
                    else
                    {
                        email.SendAttempts++;
                        await emailsDAL.UpdateEmail(email);
                    }
                }
            }
        }

        private static async Task<bool> IsSuppressed(EmailQueue email, SendGridClient client)
        {
            if (email.SuppressionGroupId == null) { return false; }

            var response = await client.GetSuppressionListAsync((int)email.SuppressionGroupId);

            return response.Contains(email.ToAddress);
        }
    }
}