﻿using System.Text.Json.Serialization;

namespace EmailService
{
    public class AppConfig
    {
        [JsonPropertyName("apiKey")]
        public string ApiKey { get; set; }

        [JsonPropertyName("emailServiceConnection")]
        public string EmailServiceConnection { get; set; }

        [JsonPropertyName("logFileLocation")]
        public string LogFileLocation { get; set; }
    }
}