﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;
using Xperience.SendGridClient.Models;

namespace Xperience.SendGridClient
{
    public class SendGridClient
    {
        private HttpClient _client;
        private readonly string _apiKey;

        public SendGridClient(HttpClient client, string apiKey)
        {
            _client = client ?? throw new ArgumentNullException(nameof(client));

            _apiKey = string.IsNullOrWhiteSpace(apiKey) ? throw new ArgumentNullException(nameof(apiKey)) : apiKey;
        }

        public async virtual Task<HttpResponseMessage> SendTemplateEmailAsync(TemplateEmail email)
        {
            if(email is null)
            {
                throw new ArgumentNullException(nameof(email));
            }

            var requestMessage = new HttpRequestMessage(HttpMethod.Post, URLDirectory.SendTemplateUrl);
            requestMessage.Headers.Add("Authorization", $"Bearer {_apiKey}");
            requestMessage.Content = new StringContent(JsonSerializer.Serialize(email), System.Text.Encoding.UTF8, "application/json");

            return await _client.SendAsync(requestMessage);
        }

        public async virtual Task<IEnumerable<string>> GetSuppressionListAsync(int suppressionGroupId)
        {
            var requestMessage = new HttpRequestMessage(HttpMethod.Get, URLDirectory.SuppressionGroupUrl(suppressionGroupId));
            requestMessage.Headers.Add("Authorization", $"Bearer {_apiKey}");

            var response = await _client.SendAsync(requestMessage);

            if (response.IsSuccessStatusCode)
            {
                return JsonSerializer.Deserialize<IEnumerable<string>>(await response.Content.ReadAsStringAsync());
            }

            else
            {
                throw new HttpRequestException(await response.Content.ReadAsStringAsync());
            }
        }
    }
}
