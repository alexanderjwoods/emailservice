﻿namespace Xperience.SendGridClient
{
    internal static class URLDirectory
    {
        private const string SENDTEMPLATEURL = "https://api.sendgrid.com/v3/mail/send";
        private const string SUPPRESSIONGROUPBASE = "https://api.sendgrid.com/v3/asm/groups/";
        private const string SUPPRESSIONGROUPEND = "/suppressions";

        internal static string SendTemplateUrl => SENDTEMPLATEURL;
        internal static string SuppressionGroupUrl(int suppressionGroupId) => $"{SUPPRESSIONGROUPBASE}{suppressionGroupId}{SUPPRESSIONGROUPEND}";
    }
}