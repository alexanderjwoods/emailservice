﻿using System.Text.Json.Serialization;

namespace Xperience.SendGridClient.Models
{
    public class To
    {
        [JsonPropertyName("email")]
        public string Email { get; set; }

        [JsonPropertyName("name")]
        public string Name { get; set; }
    }
}