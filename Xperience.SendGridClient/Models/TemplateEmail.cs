﻿using System;
using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace Xperience.SendGridClient.Models
{
    public class TemplateEmail
    {
        [JsonPropertyName("personalizations")]
        public List<Personalization> Personalizations { get; set; }

        [JsonPropertyName("from")]
        public From From { get; set; }

        [JsonPropertyName("template_id")]
        public string TemplateId { get; set; }

        [JsonPropertyName("asm")]
        public Asm Asm { get; set; }

        public TemplateEmail(string toEmail, string toName, string fromEmail, string fromName, string templateId, dynamic dynamicTemplateData, int suppressionGroupId)
        {
            if(suppressionGroupId < 0)
            {
                throw new ArgumentNullException(nameof(suppressionGroupId));
            }

            Personalizations = new List<Personalization>()
            {
                new Personalization()
                {
                    To = new List<To>()
                    {
                        new To()
                        {
                            Email = toEmail ?? throw new ArgumentNullException(nameof(toEmail)),
                            Name = toName
                        }
                    },
                    DynamicTemplateData = dynamicTemplateData
                }
            };

            From = new From()
            {
                Email = fromEmail,
                Name = fromName
            };

            TemplateId = templateId;

            Asm = new Asm() { GroupId = suppressionGroupId };
        }
    }
}