﻿using System.Text.Json.Serialization;

namespace Xperience.SendGridClient.Models
{
    public class Asm
    {
        [JsonPropertyName("group_id")]
        public int GroupId { get; set; }
    }
}