﻿using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace Xperience.SendGridClient.Models
{
    public class Personalization
    {
        [JsonPropertyName("to")]
        public List<To> To { get; set; }

        [JsonPropertyName("dynamic_template_data")]
        public dynamic DynamicTemplateData { get; set; }
    }
}