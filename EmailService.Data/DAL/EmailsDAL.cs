﻿using EmailService.Data.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EmailService.Data.DAL
{
    public class EmailsDAL
    {
        private EmailServiceContext _context;

        public EmailsDAL(EmailServiceContext context)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
        }

        public async virtual Task<IEnumerable<EmailQueue>> GetEmailsAsync()
        {
            return await _context.EmailQueue.ToListAsync();
        }

        public async virtual Task<FromAddress> GetFromEmailAsync(int fromId)
        {
            return await _context.FromAddresses.SingleAsync(f => f.FromAddressId == fromId);
        }

        public async virtual Task AddToHistory(History history)
        {
             _context.History.Add(history);
            await SaveChangesAsync();
        }

        public async virtual Task RemoveEmailFromQueue(EmailQueue email)
        {
            _context.EmailQueue.Remove(email);
            await SaveChangesAsync();
        }

        public async virtual Task AddToPoisonQueue(PoisonQueue email)
        {
            _context.PoisonQueue.Add(email);
            await SaveChangesAsync();
        }

        public async virtual Task UpdateEmail(EmailQueue email)
        {
            _context.EmailQueue.Update(email);
            await SaveChangesAsync();
        }

        private async Task<int> SaveChangesAsync()
        {
            return await _context.SaveChangesAsync();
        }
    }
}
