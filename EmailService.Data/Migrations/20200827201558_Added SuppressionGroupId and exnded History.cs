﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace EmailService.Data.Migrations
{
    public partial class AddedSuppressionGroupIdandexndedHistory : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "SentDateTime",
                table: "History");

            migrationBuilder.AlterColumn<string>(
                name: "EmailJson",
                table: "History",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "CreatedDateTime",
                table: "History",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "Reason",
                table: "History",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "Sent",
                table: "History",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<int>(
                name: "SuppressionGroupId",
                table: "EmailQueue",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CreatedDateTime",
                table: "History");

            migrationBuilder.DropColumn(
                name: "Reason",
                table: "History");

            migrationBuilder.DropColumn(
                name: "Sent",
                table: "History");

            migrationBuilder.DropColumn(
                name: "SuppressionGroupId",
                table: "EmailQueue");

            migrationBuilder.AlterColumn<string>(
                name: "EmailJson",
                table: "History",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(string));

            migrationBuilder.AddColumn<DateTime>(
                name: "SentDateTime",
                table: "History",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));
        }
    }
}
