﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace EmailService.Data.Migrations
{
    public partial class ChangedEmailStructureaddeddelayaddedvariablefrom : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "From",
                table: "EmailQueue");

            migrationBuilder.DropColumn(
                name: "To",
                table: "EmailQueue");

            migrationBuilder.AddColumn<DateTime>(
                name: "DelayUntil",
                table: "EmailQueue",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<int>(
                name: "FromAddressId",
                table: "EmailQueue",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "MaxRetryAttempts",
                table: "EmailQueue",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "ToAddress",
                table: "EmailQueue",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "ToDisplayName",
                table: "EmailQueue",
                nullable: false,
                defaultValue: "");

            migrationBuilder.CreateTable(
                name: "FromAddress",
                columns: table => new
                {
                    FromAddressId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Email = table.Column<string>(nullable: false),
                    DisplayName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FromAddress", x => x.FromAddressId);
                });

            migrationBuilder.CreateIndex(
                name: "IX_EmailQueue_FromAddressId",
                table: "EmailQueue",
                column: "FromAddressId");

            migrationBuilder.AddForeignKey(
                name: "FK_EmailQueue_FromAddress_FromAddressId",
                table: "EmailQueue",
                column: "FromAddressId",
                principalTable: "FromAddress",
                principalColumn: "FromAddressId",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_EmailQueue_FromAddress_FromAddressId",
                table: "EmailQueue");

            migrationBuilder.DropTable(
                name: "FromAddress");

            migrationBuilder.DropIndex(
                name: "IX_EmailQueue_FromAddressId",
                table: "EmailQueue");

            migrationBuilder.DropColumn(
                name: "DelayUntil",
                table: "EmailQueue");

            migrationBuilder.DropColumn(
                name: "FromAddressId",
                table: "EmailQueue");

            migrationBuilder.DropColumn(
                name: "MaxRetryAttempts",
                table: "EmailQueue");

            migrationBuilder.DropColumn(
                name: "ToAddress",
                table: "EmailQueue");

            migrationBuilder.DropColumn(
                name: "ToDisplayName",
                table: "EmailQueue");

            migrationBuilder.AddColumn<string>(
                name: "From",
                table: "EmailQueue",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "To",
                table: "EmailQueue",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "");
        }
    }
}
