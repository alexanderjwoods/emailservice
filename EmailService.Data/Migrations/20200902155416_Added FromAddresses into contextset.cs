﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace EmailService.Data.Migrations
{
    public partial class AddedFromAddressesintocontextset : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_EmailQueue_FromAddress_FromAddressId",
                table: "EmailQueue");

            migrationBuilder.DropForeignKey(
                name: "FK_PoisonQueues_EmailQueue_EmailId",
                table: "PoisonQueues");

            migrationBuilder.DropPrimaryKey(
                name: "PK_PoisonQueues",
                table: "PoisonQueues");

            migrationBuilder.DropPrimaryKey(
                name: "PK_FromAddress",
                table: "FromAddress");

            migrationBuilder.RenameTable(
                name: "PoisonQueues",
                newName: "PoisonQueue");

            migrationBuilder.RenameTable(
                name: "FromAddress",
                newName: "FromAddresses");

            migrationBuilder.RenameIndex(
                name: "IX_PoisonQueues_EmailId",
                table: "PoisonQueue",
                newName: "IX_PoisonQueue_EmailId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_PoisonQueue",
                table: "PoisonQueue",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_FromAddresses",
                table: "FromAddresses",
                column: "FromAddressId");

            migrationBuilder.AddForeignKey(
                name: "FK_EmailQueue_FromAddresses_FromAddressId",
                table: "EmailQueue",
                column: "FromAddressId",
                principalTable: "FromAddresses",
                principalColumn: "FromAddressId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_PoisonQueue_EmailQueue_EmailId",
                table: "PoisonQueue",
                column: "EmailId",
                principalTable: "EmailQueue",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_EmailQueue_FromAddresses_FromAddressId",
                table: "EmailQueue");

            migrationBuilder.DropForeignKey(
                name: "FK_PoisonQueue_EmailQueue_EmailId",
                table: "PoisonQueue");

            migrationBuilder.DropPrimaryKey(
                name: "PK_PoisonQueue",
                table: "PoisonQueue");

            migrationBuilder.DropPrimaryKey(
                name: "PK_FromAddresses",
                table: "FromAddresses");

            migrationBuilder.RenameTable(
                name: "PoisonQueue",
                newName: "PoisonQueues");

            migrationBuilder.RenameTable(
                name: "FromAddresses",
                newName: "FromAddress");

            migrationBuilder.RenameIndex(
                name: "IX_PoisonQueue_EmailId",
                table: "PoisonQueues",
                newName: "IX_PoisonQueues_EmailId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_PoisonQueues",
                table: "PoisonQueues",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_FromAddress",
                table: "FromAddress",
                column: "FromAddressId");

            migrationBuilder.AddForeignKey(
                name: "FK_EmailQueue_FromAddress_FromAddressId",
                table: "EmailQueue",
                column: "FromAddressId",
                principalTable: "FromAddress",
                principalColumn: "FromAddressId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_PoisonQueues_EmailQueue_EmailId",
                table: "PoisonQueues",
                column: "EmailId",
                principalTable: "EmailQueue",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
