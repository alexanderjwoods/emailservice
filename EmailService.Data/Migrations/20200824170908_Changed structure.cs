﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace EmailService.Data.Migrations
{
    public partial class Changedstructure : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "TemplateDataJson",
                table: "Queue");

            migrationBuilder.AlterColumn<string>(
                name: "TemplateId",
                table: "Queue",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AddColumn<string>(
                name: "TemplateData",
                table: "Queue",
                nullable: false,
                defaultValue: "");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "TemplateData",
                table: "Queue");

            migrationBuilder.AlterColumn<int>(
                name: "TemplateId",
                table: "Queue",
                type: "int",
                nullable: false,
                oldClrType: typeof(string));

            migrationBuilder.AddColumn<string>(
                name: "TemplateDataJson",
                table: "Queue",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "");
        }
    }
}
