﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace EmailService.Data.Migrations
{
    public partial class RefactorPoisonQueue : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_PoisonQueue_EmailQueue_EmailId",
                table: "PoisonQueue");

            migrationBuilder.DropIndex(
                name: "IX_PoisonQueue_EmailId",
                table: "PoisonQueue");

            migrationBuilder.DropColumn(
                name: "EmailId",
                table: "PoisonQueue");

            migrationBuilder.AddColumn<string>(
                name: "Email",
                table: "PoisonQueue",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Email",
                table: "PoisonQueue");

            migrationBuilder.AddColumn<int>(
                name: "EmailId",
                table: "PoisonQueue",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_PoisonQueue_EmailId",
                table: "PoisonQueue",
                column: "EmailId");

            migrationBuilder.AddForeignKey(
                name: "FK_PoisonQueue_EmailQueue_EmailId",
                table: "PoisonQueue",
                column: "EmailId",
                principalTable: "EmailQueue",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
