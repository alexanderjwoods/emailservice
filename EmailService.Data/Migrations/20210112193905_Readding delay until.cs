﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;

namespace EmailService.Data.Migrations
{
    public partial class Readdingdelayuntil : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "DelayUntil",
                table: "EmailQueue",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DelayUntil",
                table: "EmailQueue");
        }
    }
}
