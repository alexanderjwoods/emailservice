﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace EmailService.Data.Migrations
{
    public partial class AddedHistoryandPoisonQueue : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_Queue",
                table: "Queue");

            migrationBuilder.RenameTable(
                name: "Queue",
                newName: "EmailQueue");

            migrationBuilder.AddColumn<int>(
                name: "SendAttempts",
                table: "EmailQueue",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddPrimaryKey(
                name: "PK_EmailQueue",
                table: "EmailQueue",
                column: "Id");

            migrationBuilder.CreateTable(
                name: "History",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    EmailJson = table.Column<string>(nullable: true),
                    SentDateTime = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_History", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "PoisonQueues",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    EmailId = table.Column<int>(nullable: true),
                    PoisonMessage = table.Column<string>(nullable: true),
                    PoisonedDateTime = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PoisonQueues", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PoisonQueues_EmailQueue_EmailId",
                        column: x => x.EmailId,
                        principalTable: "EmailQueue",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_PoisonQueues_EmailId",
                table: "PoisonQueues",
                column: "EmailId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "History");

            migrationBuilder.DropTable(
                name: "PoisonQueues");

            migrationBuilder.DropPrimaryKey(
                name: "PK_EmailQueue",
                table: "EmailQueue");

            migrationBuilder.DropColumn(
                name: "SendAttempts",
                table: "EmailQueue");

            migrationBuilder.RenameTable(
                name: "EmailQueue",
                newName: "Queue");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Queue",
                table: "Queue",
                column: "Id");
        }
    }
}
