using System;
using System.ComponentModel.DataAnnotations;

namespace EmailService.Data.Models
{
    public class PoisonQueue
    {
        [Key]
        public int Id { get; set; }

        public string Email { get; set; }

        public string PoisonMessage { get; set; }

        public DateTime PoisonedDateTime { get; set; }
    }
}