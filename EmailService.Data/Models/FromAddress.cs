﻿using System.ComponentModel.DataAnnotations;

namespace EmailService.Data.Models
{
    public class FromAddress
    {
        [Key]
        public int FromAddressId { get; set; }

        [EmailAddress, Required]
        public string Email { get; set; }

        public string DisplayName { get; set; }
    }
}