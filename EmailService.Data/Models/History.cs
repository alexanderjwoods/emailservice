using System;
using System.ComponentModel.DataAnnotations;

namespace EmailService.Data.Models
{
    public class History
    {
        [Key]
        public int Id { get;set; }

        [Required]
        public string EmailJson { get; set; }

        [Required]
        public DateTime CreatedDateTime { get;set; }

        [Required]
        public bool Sent { get; set; }

        public string Reason { get; set; }
    }
}