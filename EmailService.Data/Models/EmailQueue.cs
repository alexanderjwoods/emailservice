﻿using System;
using System.ComponentModel.DataAnnotations;

namespace EmailService.Data.Models
{
    public class EmailQueue
    {
        [Key]
        public int Id { get; set; }

        [Required]
        public string TemplateId { get; set; }

        public int FromAddressId { get; set; }

        [Required]
        public FromAddress From { get; set; }

        [EmailAddress, Required]
        public string ToAddress { get; set; }

        [Required]
        public string ToDisplayName { get; set; }

        [Required]
        public string TemplateData { get; set; }

        [Required]
        public int SendAttempts { get; set; }

        [Required]
        public int MaxRetryAttempts { get; set; }

        public int? SuppressionGroupId { get; set; }

        public DateTime DelayUntil { get; set; }
    }
}