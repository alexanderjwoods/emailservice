﻿using EmailService.Data.Models;
using Microsoft.EntityFrameworkCore;

namespace EmailService.Data
{
    public class EmailServiceContext : DbContext
    {
        public DbSet<EmailQueue> EmailQueue { get; set; }

        public DbSet<PoisonQueue> PoisonQueue { get; set; }

        public DbSet<History> History { get; set; }

        public DbSet<FromAddress> FromAddresses { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            base.OnConfiguring(optionsBuilder.UseSqlServer("server=xposmssqlprod002.cehhm14x41hg.us-east-1.rds.amazonaws.com;Database=Xperience_EmailService_Dev;UID=XPOS01; Password=J5ooIQFN;"));
        }

        public EmailServiceContext() : base() { }

        public EmailServiceContext(DbContextOptions<EmailServiceContext> dbContextOptions) : base(dbContextOptions) { }
    }
}