using EmailService;
using EmailService.Data;
using EmailService.Data.DAL;
using EmailService.Data.Models;
using NSubstitute;
using NSubstitute.ExceptionExtensions;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;
using Xperience.SendGridClient;
using Xperience.SendGridClient.Models;

namespace EmailServiceTests
{
    public class ProgramTests : MockHttpExtensions
    {
        private EmailsDAL _emailsDAL;
        private SendGridClient _client;

        [SetUp]
        public void SetUp()
        {
            _emailsDAL = Substitute.For<EmailsDAL>(Substitute.For<EmailServiceContext>());
            _client = Substitute.For<SendGridClient>(Substitute.For<HttpClient>(), "api");
        }

        [Test]
        public void Run_ArgNull_NullDAL()
        {
            // Assert
            Assert.ThrowsAsync<ArgumentNullException>(async () => await Program.Run(
                null, 
                Substitute.For<SendGridClient>(Substitute.For<HttpClient>(), "api")));
        }

        [Test]
        public void Run_ArgNull_NullClient()
        {
            // Assert
            Assert.ThrowsAsync<ArgumentNullException>(async () => await Program.Run(Substitute.For<EmailsDAL>(Substitute.For<EmailServiceContext>()), null));
        }

        [Test]
        public async Task Run_AddsToHistoryAsUnsent_EmailIsSuppressed()
        {
            // Arrange
            var random = new Random();
            var suppressionGroupId = random.Next(1, 1000);
            var suppressedEmail = "test@test.com";

            var emailsQueue = new List<EmailQueue>()
            {
                new EmailQueue()
                {
                    ToAddress = suppressedEmail,
                    SuppressionGroupId = suppressionGroupId
                }
            };

            var history = new List<History>();

            _emailsDAL.GetEmailsAsync().Returns(emailsQueue);

            var suppressedEmails = new List<string>() { suppressedEmail };

            _emailsDAL
                .When(q => q.AddToHistory(Arg.Any<History>()))
                .Do(q => history.Add(q.Arg<History>()));

            _client.GetSuppressionListAsync(Arg.Any<int>()).Returns(suppressedEmails);

            // Act
            await Program.Run(_emailsDAL, _client);

            // Assert
            Assert.AreEqual(1, history.Count());
        }

        [Test]
        public async Task Run_AddsSentEmailToHistory_SuccessfulSend()
        {
            // Arrange
            var random = new Random();
            var suppressionGroupId = random.Next(1, 1000);

            var emailsQueue = new List<EmailQueue>()
            {
                new EmailQueue()
                {
                    ToAddress = "test@test.com",
                    TemplateData = "{}",
                    SuppressionGroupId = suppressionGroupId
                }
            };

            _emailsDAL.GetEmailsAsync().Returns(emailsQueue);

            _client.GetSuppressionListAsync(Arg.Any<int>()).Returns(new string[0]);

            _client.SendTemplateEmailAsync(Arg.Any<TemplateEmail>()).Returns(new HttpResponseMessage(HttpStatusCode.Accepted));

            _emailsDAL.GetFromEmailAsync(Arg.Any<int>()).Returns(new FromAddress());

            var history = new List<History>();

            _emailsDAL
                .When(q => q.AddToHistory(Arg.Any<History>()))
                .Do(q => history.Add(q.Arg<History>()));

            // Act
            await Program.Run(_emailsDAL, _client);

            // Assert
            Assert.IsTrue(history[0].Sent);
        }

        [Test]
        public async Task Run_AddsToPoisonQueue_EmailIsUnsentAndOutOfRetries()
        {
            // Arrange
            var poisonQueue = new List<PoisonQueue>();

            var random = new Random();
            var suppressionGroupId = random.Next(1, 1000);

            var emailsQueue = new List<EmailQueue>()
            {
                new EmailQueue()
                {
                    ToAddress = "test@test.com",
                    TemplateData = "{}",
                    SuppressionGroupId = suppressionGroupId,
                    SendAttempts = 0,
                    MaxRetryAttempts = 0
                }
            };

            _emailsDAL.GetEmailsAsync().Returns(emailsQueue);

            _client.GetSuppressionListAsync(Arg.Any<int>()).Returns(new string[0]);

            _client.SendTemplateEmailAsync(Arg.Any<TemplateEmail>()).Returns(new HttpResponseMessage(HttpStatusCode.BadRequest));

            _emailsDAL
                .When(q => q.AddToPoisonQueue(Arg.Any<PoisonQueue>()))
                .Do(q => poisonQueue.Add(q.Arg<PoisonQueue>()));

            // Act
            await Program.Run(_emailsDAL, _client);

            // Assert
            Assert.AreEqual(1, poisonQueue.Count());
        }

        [Test]
        public async Task Run_AddsExceptionToPoisonQueue_ExceptionIsThrown()
        {
            // Arrange
            var poisonQueue = new List<PoisonQueue>();

            var random = new Random();
            var suppressionGroupId = random.Next(1, 1000);

            var emailsQueue = new List<EmailQueue>()
            {
                new EmailQueue()
                {
                    ToAddress = "test@test.com",
                    TemplateData = "{}",
                    SuppressionGroupId = suppressionGroupId,
                    SendAttempts = 0,
                    MaxRetryAttempts = 0
                }
            };

            _emailsDAL.GetEmailsAsync().Returns(emailsQueue);

            _client.GetSuppressionListAsync(Arg.Any<int>()).Returns(new string[0]);

            _client.SendTemplateEmailAsync(Arg.Any<TemplateEmail>()).Throws(new Exception("Test Exception"));

            _emailsDAL
                .When(q => q.AddToPoisonQueue(Arg.Any<PoisonQueue>()))
                .Do(q => poisonQueue.Add(q.Arg<PoisonQueue>()));

            // Act
            await Program.Run(_emailsDAL, _client);

            // Assert
            Assert.AreEqual(1, poisonQueue.Count());
        }

        [Test]
        public async Task Run_DoesNotSend_DelayTimeHasNotPassed()
        {
            // Arrange
            var random = new Random();

            var emailsQueue = new List<EmailQueue>()
            {
                new EmailQueue()
                {
                    ToAddress = "test@test.com",
                    TemplateData = "{}",
                    SuppressionGroupId = random.Next(1, 1000),
                    DelayUntil = DateTime.Now.AddDays(1)
                },
                new EmailQueue()
                {
                    ToAddress = "test@test.com",
                    TemplateData = "{}",
                    SuppressionGroupId = random.Next(1, 1000)
                }
            };

            _emailsDAL.GetEmailsAsync().Returns(emailsQueue);

            _client.GetSuppressionListAsync(Arg.Any<int>()).Returns(new string[0]);

            _client.SendTemplateEmailAsync(Arg.Any<TemplateEmail>()).Returns(new HttpResponseMessage(HttpStatusCode.Accepted));

            _emailsDAL.GetFromEmailAsync(Arg.Any<int>()).Returns(new FromAddress());

            var history = new List<History>();

            _emailsDAL
                .When(q => q.AddToHistory(Arg.Any<History>()))
                .Do(q => history.Add(q.Arg<History>()));

            // Act
            await Program.Run(_emailsDAL, _client);

            // Assert
            Assert.IsTrue(history.Count == 1);
        }
    }
}