using NSubstitute;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;
using Xperience.SendGridClient.Models;

namespace Xperience.SendGridClient.Tests
{
    public class SendGridClientTests : MockEmailExtensions
    {
        private HttpClient _httpClient;

        [SetUp]
        public void Setup()
        {
            _httpClient = Substitute.For<HttpClient>();
        }

        [Test]
        public void HttpClientCtor_ArgNull_NullHttpClient()
        {
            // Arrange
            HttpClient client = null;

            // Assert
            Assert.Throws<ArgumentNullException>(() => new SendGridClient(client, "api"));
        }

        [TestCase(null)]
        [TestCase("")]
        [TestCase(" ")]
        public void HttpClientCtor_ArgNull_NullApiKey(string apiKey)
        {
            // Assert
            Assert.Throws<ArgumentNullException>(() => new SendGridClient(Substitute.For<HttpClient>(), apiKey));
        }
        
        [Test]
        public void SendTempateEmailAsync_ArgNull_NullTemplate()
        {
            // Assert
            Assert.ThrowsAsync<ArgumentNullException>(async () => await _client.SendTemplateEmailAsync(null));
        }

        [Test]
        public async Task SendTemplateEmailAsync_HttpResponseMessage_EmailTemplate()
        {
            // Arrange
            var template = new TemplateEmail(
                "toEmail",
                "toName",
                "fromEmail",
                "fromName",
                "templateId",
                "templateData",
                0);
            _httpClient = Substitute.For<HttpClient>(new MockHttpMessageHandler(string.Empty, System.Net.HttpStatusCode.Accepted));

            // Act
            var response = await _client.SendTemplateEmailAsync(template);

            // Assert
            Assert.IsInstanceOf<HttpResponseMessage>(response);
        }

        [Test]
        public async Task GetSuppressionListAsync_HttpRequestException_ResponseIsNotSuccess()
        {
            // Arrange
            var random = new Random();
            var suppressionGroupId = random.Next(1, 1000);

            _httpClient = Substitute.For<HttpClient>(new MockHttpMessageHandler(string.Empty, System.Net.HttpStatusCode.BadRequest));

            // Assert
            Assert.ThrowsAsync<HttpRequestException>(async () => await _client.GetSuppressionListAsync(suppressionGroupId));
        }

        [Test]
        public async Task GetSuppressionListAsync_ListOfEmails_SuccessfulResponse()
        {
            // Arrange
            var random = new Random();
            var suppressionGroupId = random.Next(1, 1000);
            var suppressedEmails = new List<string>()
            {
                "test@test.com"
            };

            _httpClient = Substitute.For<HttpClient>(new MockHttpMessageHandler(JsonSerializer.Serialize(suppressedEmails), System.Net.HttpStatusCode.Accepted));

            // Act
            var response = await _client.GetSuppressionListAsync(suppressionGroupId);

            // Assert
            Assert.IsInstanceOf<IEnumerable<string>>(response);
        }

        private SendGridClient _client => new SendGridClient(_httpClient, "api");
    }
}